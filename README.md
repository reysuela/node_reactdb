# node_reactdb CLOUD-2

This uses postgres database to generate the node react backend db. test

# Tables

  - User table
  - Others <TBD> 
  
# Docker

node_reactdb uses postgres 9.6 container. The image is deployed on docker hub using rsuela/node_reactdb:latest. By default, it uses port 5432 with default credentials.

### Todos

 - Write Unit test
 - Write Integration test
 - Write deployment scripts to kubernetes or helm charts