FROM postgres:9.6

COPY ./sql_scripts/* /docker-entrypoint-initdb.d/

ENV DB_USER admindb
ENV DB_PASSWORD 123qweasd!A
ENV DB_DATABASE RSDB
ENV POSTGRES_USER postgres

EXPOSE 5432
